#include <GL/glut.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <math.h>

using namespace std;

/*
*   class Properties: Uygulama boyunca kullanilacak degiskenleri saklamak icin global olarak bu class'i kullaniyorum.
*/
class Properties { 
    public: 
        int processNumber;  // kullanici tarafından secilen islem numarasi
        int numberOfEdges; // kullanici tarafından girilen kenar sayisi

        double rotateAngle; // dondurme acisi
        double translation; // oteleme miktari
    
        bool mirror = false; // aynalama islemi yapilacak mi?
        int mirrorAxis; // 1 =  x ekseni, 2 = y ekseni,  3 = orijin

        double shearing; // egme islemi icin kullanici tarafından girilen miktar 
        int shearingAxis; // 1 = x ekseni, 2 = y ekseni
 
        double scaling; // olcekleme islemi icin kullanici tarafından girilen miktar 
        double poligonCoordinates [99][99]; // her nokta icin X ve Y degerlerinin saklandigi 2d array
};

// Properties classından alınmış bir obje.
Properties prop;

/*
*   Menu yazdirma fonksiyonu.
*/
int menu(void) {
    int choose;
    printf ("*********************************************************************\n");
    printf ("bu uygulama ile istediginiz bir poligona sectiginiz islemi uygulayabilirsiniz.\n");
    printf ("*********************************************************************\n");
    printf ("\n");
    printf("1. Translation (Oteleme)\n");
    printf("2. Scaling (Olcekleme)\n");
    printf("3. Rotation (Dondurme)\n");
    printf("4. Mirror Reflection (Yansima)\n");
    printf("5. Shearing (Egme)\n");
    printf("6. Exit - CIKIS \n");
    printf ("Seciminizi Giriniz: ");
    scanf ("%d", &choose);
    return choose;
}

void displayMe(void) {
    glClear(GL_COLOR_BUFFER_BIT);

    // cizinlen orijinal nesne
    glBegin(GL_POLYGON);
    for (int x = 0; x < prop.numberOfEdges; x++) {
        glColor3f( 1, 0, 0 ); // kirmizi renk
        glVertex2f(prop.poligonCoordinates[x][0], prop.poligonCoordinates[x][1]);
        cout << prop.poligonCoordinates[x][0];
        cout << prop.poligonCoordinates[x][1];
        cout<<endl;
    }
    glEnd();


    // manipülasyon işlemleri burada basliyor
    glBegin(GL_POLYGON);
    if (prop.processNumber == 1) { // translation durumu
        for (int x = 0; x < prop.numberOfEdges; x++) {
            glColor3f( 0, 1, 0 ); 
            glVertex2f(prop.poligonCoordinates[x][0] + prop.translation, prop.poligonCoordinates[x][1] + prop.translation);
            cout << prop.poligonCoordinates[x][0] + prop.translation;
            cout << prop.poligonCoordinates[x][1] + prop.translation;
            cout<<endl;
        }
    }
    glEnd();

    glBegin(GL_POLYGON);
        if (prop.processNumber == 2) { // scaling durumu
            for (int x = 0; x < prop.numberOfEdges; x++) {
                glColor3f( 0, 1, 0 );
                glVertex2f(prop.poligonCoordinates[x][0] * prop.scaling, prop.poligonCoordinates[x][1] * prop.scaling);
                cout << prop.poligonCoordinates[x][0] + prop.scaling;
                cout << prop.poligonCoordinates[x][1] + prop.scaling;
                cout<<endl;
            }
        }
    glEnd();

    glBegin(GL_POLYGON);
    glLoadIdentity();
        if (prop.processNumber == 3) { // rotation durumu
            glColor3f(1,0,1);
            for (int x = 0; x < prop.numberOfEdges; x++) {
                glVertex2i(round((prop.poligonCoordinates[x][0] * cos(prop.rotateAngle)) - (prop.poligonCoordinates[x][1] * sin(prop.rotateAngle))), 
                                     round((prop.poligonCoordinates[x][0] * sin(prop.rotateAngle)) + (prop.poligonCoordinates[x][1] * cos(prop.rotateAngle))));
                cout << round((prop.poligonCoordinates[x][0] * cos(prop.rotateAngle)) - (prop.poligonCoordinates[x][1] * sin(prop.rotateAngle)));
                cout << round((prop.poligonCoordinates[x][0] * sin(prop.rotateAngle)) + (prop.poligonCoordinates[x][1] * cos(prop.rotateAngle)));
                cout<<endl;
            }
        }
    glEnd();

        glBegin(GL_POLYGON);
        if (prop.mirror) { // mirroring durumu
            if (prop.mirrorAxis == 1) { // x eksenine gore
                for (int x = 0; x < prop.numberOfEdges; x++) {
                    glColor3f( 0, 1, 0 );
                    glVertex2f(prop.poligonCoordinates[x][0], 0.0 - prop.poligonCoordinates[x][1]);
                    cout << prop.poligonCoordinates[x][0];
                    cout << 0.0 -  prop.poligonCoordinates[x][1];
                    cout<<endl;
                }
            } else if (prop.mirrorAxis == 2) { // y eksenine gore
                for (int x = 0; x < prop.numberOfEdges; x++) {
                    glColor3f( 0, 1, 0 );
                    glVertex2f(0.0 - prop.poligonCoordinates[x][0], prop.poligonCoordinates[x][1]);
                    cout << 0.0 - prop.poligonCoordinates[x][0];
                    cout << prop.poligonCoordinates[x][1];
                    cout<<endl;
                }
            } else if (prop.mirrorAxis == 3) { // orijine gore
                for (int x = 0; x < prop.numberOfEdges; x++) {
                    glColor3f( 0, 1, 0 );
                    glVertex2f(0.0 - prop.poligonCoordinates[x][0], 0.0 - prop.poligonCoordinates[x][1]);
                    cout << 0.0 - prop.poligonCoordinates[x][0];
                    cout << 0.0 - prop.poligonCoordinates[x][1];
                    cout<<endl;
                }
            }
            else {
                printf("Command not found!!!! - EXIT");
            }
        }
    glEnd();

    if (prop.processNumber == 5) { // shearing durumu
        glColor3f(0.0, 0.0, 1.0);

        if (prop.shearingAxis == 1)
        {
                 glVertex2i(prop.poligonCoordinates[0][0], prop.poligonCoordinates[0][1]);
                glVertex2i(prop.poligonCoordinates[1][0] + prop.shearing, prop.poligonCoordinates[1][1]);
                glVertex2i(prop.poligonCoordinates[2][0] + prop.shearing, prop.poligonCoordinates[2][1]);
                glVertex2i(prop.poligonCoordinates[3][0], prop.poligonCoordinates[3][1]);
        }
        else if (prop.shearingAxis == 2)
        {
            glVertex2i(prop.poligonCoordinates[0][0], prop.poligonCoordinates[0][1]);
            glVertex2i(prop.poligonCoordinates[1][0], prop.poligonCoordinates[1][1]);
            glVertex2i(prop.poligonCoordinates[2][0], prop.poligonCoordinates[2][1] + prop.shearing);
            glVertex2i(prop.poligonCoordinates[3][0], prop.poligonCoordinates[3][1] + prop.shearing);
        }
    }


    glFlush();
}

int main(int argc, char** argv) {
    // menu fonksiyonundan donen degeri class objesi icinde sakliyorum.
    prop.processNumber = menu();

    if (prop.processNumber == 1) {
        printf ("Öteleme uzaklığını giriniz: ");
        scanf ("%lf", &prop.translation);
    }
    else if (prop.processNumber == 2) {
        printf ("Olcekleme degerini giriniz: ");
        scanf ("%lf", &prop.scaling);
    }
    else if (prop.processNumber == 3) {
        printf ("Döndürme açısını giriniz: ");
        scanf ("%lf", &prop.rotateAngle);
    }
    else if (prop.processNumber == 4) {
        prop.mirror = true;
        printf ("Hangi eksene göre yasıma uygulamak istediğinizi seciniz: (x = 1, y = 2, o = 3):");
        scanf ("%d", &prop.mirrorAxis);
    }
    else if (prop.processNumber == 5) {
        printf ("Egme değerini giriniz: ");
        scanf ("%lf", &prop.shearing);
        printf ("Hangi eksene göre egme uygulamak istediğinizi seciniz: (x = 1, y = 2):");
        scanf ("%d", &prop.shearingAxis);
    } 
    else {
            printf ("command not found! // EXIT\n");
            return 0;
    }

    printf ("Poligonun kenar sayisi: ");
    scanf ("%d", &prop.numberOfEdges);

    // obje icindeki matrix'in init edilmesi.
    prop.poligonCoordinates [prop.numberOfEdges-1][2];
    
    // obje icindeki matrix'e her bir nokta icin X ve Y degerlerini kullanicidan alıp sakliyorum. 
    for (int x = 0; x < prop.numberOfEdges; x++){
        for (int y = 0; y < 2; y++) {
                if (y == 0) {
                    printf ("%d . noktanin koordinatlari (x): ", x+1);
                    scanf ("%lf", &prop.poligonCoordinates[x][0]);
                } else {
                    printf ("%d . noktanin koordinatlari (y): ", x+1);
                    scanf ("%lf", &prop.poligonCoordinates[x][1]);
                }
            cout << prop.poligonCoordinates[x][y];
            cout<<endl;
        }
    cout<<endl;
    }

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE);
    glutInitWindowSize(700, 700);
    glutInitWindowPosition(1100, 300);
    glutCreateWindow("Bilgisayar Grafikleri - Odev");
    glutDisplayFunc(displayMe);
    glutMainLoop();
    
    return 0;
}
